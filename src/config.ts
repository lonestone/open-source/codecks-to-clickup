const config = {
  clickUpConfig: {
    domain: "https://api.clickup.com/v1/",
    officialApiDomain: "https://api.clickup.com/api/v1/",
    publicKey: "user public key",
    bearerToken: "user bearer token (get it in browser)"
  },
  codecksConfig: {
    subdomain: "team subdomain",
    token: "user token (get it in browser)",
    adminId: 0,
    accountId: 0
  }
};

export default config;
