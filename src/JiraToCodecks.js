const config = require('./config.json');
import _ from 'lodash';

// Jira
import JiraApi from './JiraApi' ;

// Jira configuration
const jira = new JiraApi(config.jiraConfig.domain, config.jiraConfig.token);

// Codecks
import CodecksAPI from './lib/node-codecks/CodecksAPI';
const codecks = new CodecksAPI(config.codecksConfig.subdomain, config.codecksConfig.token);

const JiraToCodecksHelper = {

  /**
   *
   * @returns {Promise.<string>}
   * @constructor
   */
  async ExportToCodecks () {

    console.log('Starting');

    let jiraProjects = [];
    let jiraEpics = {};
    let jiraSprints = {};
    let jiraVersions = {};
    let jiraTasks = {};

    let codecksJiraProjectDict = {};
    let epicDeckDict = {};
    let versionMilestoneDict = {};
    let taskCardDict = {};

    let jiraCodecksUserDict = {};
    let userRefDict = {};
    config.users.forEach(function (user) {
      jiraCodecksUserDict[ user.jiraKey ] = user.codecksId;
      userRefDict[ '[~' + user.jiraKey + ']' ] = '@[userId:' + user.codecksId + ']';
    });

    let userRefRegex = new RegExp(_.map(Object.keys(userRefDict), function (k) {
      return _.escapeRegExp(k)
    }).join("|"), "ig");

    // 1st Step : we get the information we need
    // We get all the Jira projects and all statuses
    await jira.getProjects().then(ps => jiraProjects = ps);

    console.log("We have all the projects from Jira");

    // 2nd Step : for each project we get its
    // Epics
    // Versions
    // Sprints
    // Tasks

    for (let projectIndex = 0; projectIndex < jiraProjects.length; projectIndex++) {
      let project = jiraProjects[ projectIndex ];

      await Promise.all([
        jira.getEpics(project.key).then(es => jiraEpics[ projectIndex ] = es),
        jira.getProjectVersions(project.key).then(vs => jiraVersions[ projectIndex ] = vs),
        jira.getSprints(project.key).then(sps => jiraSprints[ projectIndex ] = sps),
        jira.getTasks(project.key).then(tks => jiraTasks[ projectIndex ] = tks)
      ]);

      console.log("We have all the elements from Jira for this project");

      // Project object
      let codecksProject = {
        userId: config.codecksConfig.adminId,
        name: project.name
      };

      // Creating project on codecks
      await codecks.API.mutate.projects.create(codecksProject).then(res => {codecksProject.id = res.id});
      codecksJiraProjectDict[ codecksProject.id ] = project.id;

      console.log("We created the new Codecks Project");

      // Creating decks in this project
      if (jiraEpics[ projectIndex ].length !== 0) {
        for (let epic of jiraEpics[ projectIndex ]) {

          let deck = {
            content: epic.fields.customfield_10006,
            coverFileData: null,
            projectId: codecksProject.id,
            userId: config.codecksConfig.adminId
          };

          await codecks.API.mutate.decks.create(deck).then(res => {deck.id = res.id});
          epicDeckDict[ epic.key ] = deck.id;
        }
      }

      // Creating an deck for cards without epics
      let deck = {
        content: "Cards to sort",
        coverFileData: null,
        projectId: codecksProject.id,
        userId: config.codecksConfig.adminId
      };

      await codecks.API.mutate.decks.create(deck).then(res => {deck.id = res.id});
      epicDeckDict[ 'messy' ] = deck.id;

      console.log("We created the decks");

      // TODO : Set-up the milestone from sprint
      // Creating milestones from sprint
      /*    for (let [ index, sprint ] in jiraSprints) {
       let milestone = {
       accountId: config.codecksConfig.accountId,
       color: "#00c295",
       date: sprint.endDate,
       description: sprint.name,
       name: sprint.name,
       projectId: codecksProject.id,
       userId: config.codecksConfig.adminId
       };

       await codecks.API.mutate.milestones.create(milestone).then(res => {milestone.id = res.id});
       sprintMilestoneDict[ sprint.id ] = milestone.id;
       }*/

      // Creating milestones from versions
      for (let version of jiraVersions[ projectIndex ]) {
        let milestone = {
          accountId: config.codecksConfig.accountId,
          color: "#c20800",
          date: version.releaseDate,
          description: version.description,
          name: version.name,
          projectId: codecksProject.id,
          userId: config.codecksConfig.adminId
        };

        await codecks.API.mutate.milestones.create(milestone).then(res => {milestone.id = res.id});
        versionMilestoneDict[ version.id ] = milestone.id;
      }

      console.log("We created the milestones");

      // Finally generating the cards
      for (let task of jiraTasks[ projectIndex ]) {
        let milestoneId = null;
        let assigneeId = null;
        let deckId = epicDeckDict[ 'messy' ];
        let content = task.fields.summary + " \n\n";

        if (task.fields.assignee !== null && task.fields.assignee.key !== '') {
          assigneeId = jiraCodecksUserDict[ task.fields.assignee.key ];
        }

        content += task.fields.description != null ? task.fields.description : '';

        // Testing if sub-task
        if ('parent' in task.fields) {
          // In the case of a sub-tasks we need to get its parent from the tasks list for the details like version and epic
          let parent = jiraTasks[ projectIndex ].find(function (t) {
            return t.id === task.fields.parent.id;
          });

          content = parent.fields.summary + ' - ' + content;
          if (parent.fields.customfield_10004 !== null) deckId = epicDeckDict[ parent.fields.customfield_10004 ];

          if (parent.fields.fixVersions !== null && Array.isArray(parent.fields.fixVersions) && parent.fields.fixVersions.length != 0) {
            milestoneId = versionMilestoneDict[ parent.fields.fixVersions[ 0 ].id ];
          }
        } else {
          if (task.fields.fixVersions !== null && Array.isArray(task.fields.fixVersions) && task.fields.fixVersions.length != 0) {
            milestoneId = versionMilestoneDict[ task.fields.fixVersions[ 0 ].id ];
          }
          if (task.fields.customfield_10004 !== null) deckId = epicDeckDict[ task.fields.customfield_10004 ];
        }

        // Finally we convert the jira user references to codecks ones
        content = content.replace(userRefRegex, function (m) {
          return userRefDict[ m ];
        });

        // We set the status. This could be handled better. Config.json is matching statuses
        // Nobody assigned, task not started = unassigned
        // Nobody assigned, task started = we assign the admin and set status to started
        // Nobody assigned, task pending review = we assign the admin and status = review
        // Nobody assigned, task blocked = we assign the admin and status = blocked
        // Nobody assigned, task finished = we assign the admin and status = done
        // Assignee, task not started = not_started
        // Assignee, task started = started
        // Assignee, task pending review = review
        // Assignee, task blocked = blocked
        // Assignee, task done = done

        let status = 'unassigned';
        let jiraStatusKey = task.fields.status.name;
        if (jiraStatusKey !== null && jiraStatusKey in config.status) {
          status = config.status[ jiraStatusKey ];
          if (status !== 'unassigned' && assigneeId === null) assigneeId = config.codecksConfig.adminId;
          else if (status === 'unassigned' && assigneeId !== null) status = 'not_started';
        }

        let card = {
          assigneeId: assigneeId,
          attachments: [],
          content: content,
          deckId: deckId,
          id: "new",
          masterTags: task.fields.labels,
          milestoneId: milestoneId,
          priority: null,
          projectId: codecksProject.id,
          sessionId: "Bot",
          status: "unassigned",
          userId: config.codecksConfig.adminId
        };

        await codecks.API.mutate.cards.create(card).then(res => {card.id = res.id});

        // We need to update the status after the card creation
        // Reassigning the same status throws an error
        if (status === 'review' || status === 'blocked') {
          await codecks.API.mutate.resolvables.create({
            cardId: card.id,
            context: status,
            userId: assigneeId,
            content: ''
          });
        } else if (status !== 'unassigned') {
          await codecks.API.mutate.cards.update({ id: card.id, status: status })
        }

        // Finally we archive the card if the task was done more than 48h ago
        if (task.fields.resolutiondate !== null) {
          let elapsed = new Date(Date.now()) - new Date(task.fields.resolutiondate);
          let hours = elapsed / 1000 / 60 / 60;
          if (hours >= 48) await codecks.API.mutate.cards.update({
            id: card.id,
            visibility: 'archived'
          })
        }

        taskCardDict[ task.id ] = card.id;
      }
      console.log("We created the cards");
    }
    return 'All projects Created';
  }
};

export default JiraToCodecksHelper;