"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
let sortedStatusVals = (exports.sortedStatusVals = [
  "unassigned",
  "assigned",
  "started",
  "snoozing",
  "blocked",
  "review",
  "done",
  "archived",
  "deleted"
]);

let statusToSortVal = sortedStatusVals.reduce((memo, val, i) => {
  memo[val] = i;
  return memo;
}, {});

let statusColors = (exports.statusColors = {
  snoozing: "#7d1274",
  blocked: "#c01b1b",
  assigned: "#a8925d",
  unassigned: "#828282",
  review: "#159590",
  done: "#31d367",
  started: "#2569ad",
  archived: "#291600",
  deleted: "#43282a",
  not_started: "#828282"
});

// tslint:disable-next-line:no-shadowed-variable
let statusForCard = (exports.statusForCard = function statusForCard(card) {
  if (card.visibility === "archived") {
    return "archived";
  }
  if (card.visibility === "deleted") {
    return "deleted";
  }
  if (card.$meta.exists("openBlockResolvables")) {
    return "blocked";
  }
  if (card.$meta.exists("openReviewResolvables")) {
    return "review";
  }
  if (card.status === "not_started") {
    return card.assignee ? "assigned" : "unassigned";
  }
  return card.status;
});

let statusToLabel = (exports.statusToLabel = {
  snoozing: "Snoozing",
  blocked: "Blocked",
  assigned: "Assigned",
  unassigned: "Unassigned",
  review: "Review",
  done: "Done",
  started: "Started",
  archived: "Archived",
  deleted: "Deleted",
  not_started: "Not started"
});

// x cards are [PHRASE]
let statusToPhrase = (exports.statusToPhrase = {
  snoozing: "snoozing",
  blocked: "blocked",
  assigned: "assigned",
  unassigned: "unassigned",
  review: "in review",
  done: "done",
  started: "started",
  archived: "archived",
  deleted: "deleted",
  not_started: "not started"
});

// tslint:disable-next-line:no-shadowed-variable
let statusForCardLabel = (exports.statusForCardLabel = function statusForCardLabel(
  card
) {
  return statusToLabel[statusForCard(card)];
});
