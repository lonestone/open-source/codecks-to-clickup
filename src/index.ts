import { ClickUp } from "./ClickUpAPI";
import { ITaskCreation, Priorities, Status } from "./ClickUpInterfaces";
import CodecksAPI from "./lib/node-codecks/CodecksAPI";
import config from "./config";

const codecks = new CodecksAPI(
  config.codecksConfig.subdomain,
  config.codecksConfig.token
);

const characterDecks = [
  "Mr Loyal",
  "Engineer",
  "Civil",
  "Leader",
  "Gunner",
  "Hooligan",
  "Brute",
  "Amazone",
  "Medic",
  "Ranger",
  "Scout"
];

const zombieDecks = [
  "Zombie Boss",
  "Zombie Yellow",
  "Zombie Shield",
  "Zombie Strong",
  "Zombie Fat",
  "Zombie Fast",
  "Zombie Dog",
  "Zombie Basic"
];

const mapDecks = [
  "Map_Fountain (Quest)",
  "Back To School",
  "CampMap BETA",
  "Map_Ossuary (Quest)",
  "Map_Let id die",
  "Map_Cinemass Presents",
  "Map_Gyver Street",
  "JailBreak",
  "Canned Brain",
  "HardWork",
  "Map_Easy Landing (Quest)",
  "LastBottle 4K",
  "CampMap Alpha",
  "SubLavaWay 4k",
  "HungryCops 4k",
  "SubLavaWay",
  "HungryCops",
  "LastBottle",
  "GreenDen 4k",
  "GreenDen"
];

const itemDecks = [
  "Refonte Armes",
  "Refonte Armures",
  "Slash2Handed",
  "Slash",
  "Blunt",
  "Sniper",
  "Pistol",
  "Blunt",
  "Shotgun",
  "Automatic"
];

const goalDecks = [
  "Stand Your Ground",
  "Power Grid",
  "Arena - Crowd Favorite",
  "Kill The Boss",
  "Conquer&Fortify",
  "Zone Capture",
  "BombAttack"
];

interface ICodecksCard {
  cardId: string;
  deckId: string;
  projectId: string;
  priority: any | null;
  title: string;
  content: string;
  tags: string[];
  status: string;
  effort: number | null;
  assignee: { id: string; emails: Array<{ email: string }> };
  openBlockResolvables: any;
  openReviewResolvables: any;
  visibility: string;
}

async function migrate() {
  const clickUp = new ClickUp(
    config.clickUpConfig.publicKey,
    config.clickUpConfig.bearerToken
  );
  const teams = await clickUp.getTeams();

  const loneStoneTeam = teams.find(t => t.name === "Lone Stone");
  if (!loneStoneTeam) {
    throw new Error("Team not found");
  }

  const cityInvadersSpace = (await clickUp.getTeamSpaces(
    loneStoneTeam.id
  )).find(s => s.name === "City Invaders");
  if (!cityInvadersSpace) {
    throw new Error("Space not found");
  }

  const alphaProject = (await clickUp.getSpaceProjects(
    cityInvadersSpace.id
  )).find(p => p.name === "Alpha");
  if (!alphaProject) {
    throw new Error("Project not found");
  }

  const clickUpUsers = await clickUp.getTeamUsers(loneStoneTeam.id);

  // First we create a few Lists if needed
  const miscList =
    alphaProject.lists.find(l => l.name === "Misc") ||
    (await clickUp.createList({ name: "Misc" }, alphaProject.id));

  const charList =
    alphaProject.lists.find(l => l.name === "Characters") ||
    (await clickUp.createList({ name: "Characters" }, alphaProject.id));

  const itemsList =
    alphaProject.lists.find(l => l.name === "Items") ||
    (await clickUp.createList({ name: "Items" }, alphaProject.id));

  const mapsList =
    alphaProject.lists.find(l => l.name === "Maps") ||
    (await clickUp.createList({ name: "Maps" }, alphaProject.id));

  const goalsList =
    alphaProject.lists.find(l => l.name === "Goals") ||
    (await clickUp.createList({ name: "Goals" }, alphaProject.id));

  const zList =
    alphaProject.lists.find(l => l.name === "Zombies") ||
    (await clickUp.createList({ name: "Zombies" }, alphaProject.id));

  // Then we GET the cards and decks and stuff
  const codecksProject = await codecks.fetch({
    "project(255)": [
      "name",
      "exists:openResolvables",
      {
        anyCards: [
          "deckId",
          "priority",
          "content",
          "status",
          "tags",
          "mentionedUsers",
          "effort",
          "title",
          "visibility",
          { assignee: ["id", "name", { emails: ["email"] }] },
          { openBlockResolvables: [] },
          { openReviewResolvables: [] }
        ],
        decks: ["id", "title"],
        users: ["name", { emails: ["email"] }]
      }
    ]
  });

  const codecksUsers = codecksProject.project["255"].users as Array<{
    name: string;
    emails: Array<{ email: string }>;
  }>;

  const codecksCards = codecksProject.project["255"].anyCards as ICodecksCard[];

  const codecksDecks = codecksProject.project["255"].decks as Array<{
    id: string;
    title: string;
    projectId: string;
  }>;

  for (const deck of codecksDecks) {
    // Deck cards = Subtasks
    const cards = codecksCards.filter(
      c => c.deckId === deck.id && c.visibility !== "deleted"
    );

    const subtasks: ITaskCreation[] = [];

    for (const card of cards) {
      const assignee = card.assignee
        ? clickUpUsers.find(u => card.assignee.emails[0].email === u.email)
        : null;

      const cardAsTask: ITaskCreation = {
        name: card.title,
        content: card.content,
        priority: codecksToClickupPriority(card.priority),
        status: codecksToClickupStatus(card),
        assignees: assignee ? [assignee.id.toString()] : undefined
      };

      subtasks.push(cardAsTask);
    }

    // Deck = Task
    const deckAsTask: ITaskCreation = {
      name: deck.title,
      subtasks
    };

    let listId = miscList.id;

    if (characterDecks.includes(deck.title)) {
      listId = charList.id;
    } else if (zombieDecks.includes(deck.title)) {
      listId = zList.id;
    } else if (mapDecks.includes(deck.title)) {
      listId = mapsList.id;
    } else if (itemDecks.includes(deck.title)) {
      listId = itemsList.id;
    } else if (goalDecks.includes(deck.title)) {
      listId = goalsList.id;
    }

    await clickUp.createTask(deckAsTask, listId);
  }
}

const codecksToClickupStatus = (card: ICodecksCard): Status => {
  if (card.visibility === "archived") {
    return Status.Closed;
  }
  if (
    card.openBlockResolvables != null &&
    card.openBlockResolvables.length !== 0
  ) {
    return Status.Blocked;
  }
  if (
    card.openReviewResolvables != null &&
    card.openReviewResolvables.length !== 0
  ) {
    return Status.InReview;
  }
  if (card.status === "not_started") {
    return Status.Open;
  }
  if (card.status === "started" || card.status === "snoozing") {
    return Status.InProgress;
  }
  if (card.status === "done") {
    return Status.Closed;
  }
  return Status.Open;
};

const codecksToClickupPriority = (priority: string | null): Priorities => {
  switch (priority) {
    case "a":
      return Priorities.Urgent;

    case "b":
      return Priorities.High;

    case "c":
      return Priorities.Normal;

    default:
      return Priorities.Low;
  }
};

migrate().catch(e => console.error(e));
