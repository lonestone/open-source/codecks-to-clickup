# Codecks.io Exporter to ClickUp.com

This is a work in progress tool, allowing you to export your Codecks projects to ClikUp.

## How it works

This is fairly simple.

- All Codecks decks are transformed into tasks on ClickUp side
- Assignee (matched by email), status, priority, content and title are saved
- Milestones, Tags and comments are not saved
- Cards are created as sub-tasks of the previously created task

I create several list in a arbitrary way to better sort the newly created tasks.

## Config

The config.json file contains:

- ClickUp and Codecks security configuration (tokens)

You will need to complete this configuration manually before launching the tool. In the future the configuration may be done with a dedicated UI.

## Limitations

- This project has not (yet) been tested on a lot of projects
- Milestones, Tags, attachements and comments are not exported
